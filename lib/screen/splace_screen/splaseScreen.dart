import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../home/homePage.dart'; // Mengimpor file main.dart untuk digunakan dalam penavigasian.

// Kelas SplashScreen adalah StatefulWidget yang menampilkan layar pembuka aplikasi (splash screen).
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late BuildContext splashContext;

  @override
  void initState() {
    super.initState();
    checkFirstScreen(); // Memanggil fungsi checkFirstScreen saat initState dipanggil.
  }

  // Fungsi untuk menampilkan layar splash screen selama beberapa detik sebelum beralih ke halaman utama.
  Future<void> checkFirstScreen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance(); // Mendapatkan instance SharedPreferences.
    int countOpen = (prefs.getInt('countOpen') ?? 0); // Mendapatkan nilai countOpen dari SharedPreferences.
    if (countOpen >= 0) {
      // Jika countOpen lebih besar atau sama dengan 0, maka:
      Future.delayed(const Duration(seconds: 5), () {
        // Membuat penundaan sebelum beralih ke halaman berikutnya.
        Get.off(() => const HomePage(), transition: Transition.fadeIn); // Berpindah ke halaman HomePage menggunakan GetX.
      });
      await prefs.setInt('countOpen', countOpen + 1); // Menyimpan nilai countOpen ke SharedPreferences dengan menambah 1.
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size; // Mendapatkan ukuran layar perangkat.
    splashContext = context; // Menyimpan konteks dalam splashContext.
    return Scaffold(
      // Scaffold sebagai kerangka UI.
      body: Container(
        // Container untuk mengatur tata letak elemen.
        width: double.infinity, // Lebar container mengisi layar penuh.
        height: size.height, // Tinggi container mengikuti tinggi layar.
        decoration: const BoxDecoration(
          // Mendekorasi container dengan gradient.
          gradient: LinearGradient(
            colors: [Colors.blue, Colors.white], // Warna gradient dari biru ke putih.
            begin: Alignment.topLeft, // Posisi awal gradient di pojok kiri atas.
            end: Alignment.bottomRight, // Posisi akhir gradient di pojok kanan bawah.
            stops: [0.0, 1.0], // Pemberhentian gradient.
          ),
        ),
        child: Center(
          // Menempatkan konten ke tengah.
          child: Column(
            // Column untuk menata konten secara vertikal.
            mainAxisAlignment: MainAxisAlignment.center, // Memusatkan konten secara vertikal.
            children: [
              Image.network(
                // Menampilkan gambar dari jaringan.
                'https://cdn-icons-png.flaticon.com/128/10473/10473465.png', // URL gambar.
                width: 130, // Lebar gambar.
                errorBuilder: (context, error, stackTrace) {
                  // Handler jika terjadi kesalahan dalam memuat gambar.
                  return const Icon(Icons.error); // Mengembalikan ikon kesalahan.
                },
              ),
              SizedBox(
                height: size.height * 0.05, // Memberi jarak antara gambar dan indikator loading.
              ),
              const Center(
                // Memusatkan indikator loading.
                child: CircularProgressIndicator(
                  color: Colors.black, // Warna indikator loading.
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
