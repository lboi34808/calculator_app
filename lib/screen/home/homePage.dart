import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

import '../../component/widget/buttonWidget.dart';

// Kelas HomePage adalah StatefulWidget yang menampilkan kalkulator dengan antarmuka pengguna.
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // Variabel untuk menyimpan teks yang ditampilkan di layar kalkulator.
  String screenText = '0';

  @override
  Widget build(BuildContext context) {
    // Scaffold sebagai kerangka UI.
    return Scaffold(
      body: Column(
        children: [
          // Bagian atas layar kalkulator.
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.blue, Colors.white],
                  stops: [0.0, 1.0],
                ),
              ),
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  screenText,
                  style: Theme.of(context).textTheme.displaySmall!.copyWith(
                    color: Theme.of(context).primaryColorDark,
                  ),
                ),
              ),
            ),
          ),
          // Grid tombol-tombol kalkulator.
          GridView.count(
            crossAxisCount: 4,
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            children: <Widget>[
              // Tombol untuk menghapus semua teks di layar.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorLight,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: 'C',
                onTap: () {
                  setState(() {
                    screenText = '0';
                  });
                },
              ),
              // Tombol untuk mengubah tanda bilangan.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '+/-',
                onTap: () {
                  performOperation('negate');
                },
              ),
              // Tombol untuk persentase.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '%',
                onTap: () {
                  performOperation('%');
                },
              ),
              // Tombol untuk menghapus karakter terakhir di layar.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: 'Backspace',
                icon: Icons.backspace,
                onTap: () {
                  setState(() {
                    if (screenText == '0' ||
                        screenText == '' ||
                        screenText.length == 1) {
                      screenText = '0';
                    } else {
                      screenText =
                          screenText.substring(0, screenText.length - 1);
                    }
                  });
                },
              ),
              // Tombol-tombol angka.
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '7',
                onTap: () {
                  pressNumber(7);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '8',
                onTap: () {
                  pressNumber(8);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '9',
                onTap: () {
                  pressNumber(9);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '/',
                onTap: () {
                  performOperation('/');
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '4',
                onTap: () {
                  pressNumber(4);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '5',
                onTap: () {
                  pressNumber(5);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '6',
                onTap: () {
                  pressNumber(6);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '*',
                onTap: () {
                  performOperation('*');
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '1',
                onTap: () {
                  pressNumber(1);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '2',
                onTap: () {
                  pressNumber(2);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '3',
                onTap: () {
                  pressNumber(3);
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '-',
                onTap: () {
                  performOperation('-');
                },
              ),
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '0',
                onTap: () {
                  pressNumber(0);
                },
              ),
              // Tombol untuk desimal point.
              ButtonWidgetCalculator(
                backgroundColor: Colors.white,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '.',
                onTap: () {
                  setState(() {
                    if (!screenText.contains('.')) {
                      screenText = '$screenText.';
                    }
                  });
                },
              ),
              // Tombol untuk menghitung hasil.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorLight,
                foregroundColor: Theme.of(context).primaryColorDark,
                text: '=',
                onTap: () {
                  setState(() {
                    evaluateExpression();
                  });
                },
              ),
              // Tombol untuk penjumlahan.
              ButtonWidgetCalculator(
                backgroundColor: Theme.of(context).primaryColorDark,
                foregroundColor: Theme.of(context).primaryColorLight,
                text: '+',
                onTap: () {
                  performOperation('+');
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  // Fungsi untuk menambahkan angka ke layar.
  void pressNumber(int number) {
    setState(() {
      if (screenText == '0') {
        screenText = '$number';
      } else {
        screenText = '$screenText$number';
      }
    });
  }

  // Fungsi untuk melakukan operasi matematika.
  void performOperation(String operator) {
    setState(() {
      if (operator == 'negate') {
        if (screenText != '0') {
          if (screenText.startsWith('-')) {
            screenText = screenText.substring(1);
          } else {
            screenText = '-$screenText';
          }
        }
      } else {
        screenText = '$screenText $operator ';
      }
    });
  }

  // Fungsi untuk mengevaluasi ekspresi matematika.
  void evaluateExpression() {
    setState(() {
      try {
        final parser = Parser();
        final parsedExpression = parser.parse(screenText);
        final ContextModel cm = ContextModel();
        final result = parsedExpression.evaluate(EvaluationType.REAL, cm);
        screenText = result.toString();
      } catch (e) {
        screenText = 'Error';
      }
    });
  }
}
