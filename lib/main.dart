import 'package:calculator/screen/splace_screen/splaseScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}


//Ini adalah file main.dart yang berfungsi sebagai titik masuk (entry point) dari aplikasi Flutter. Di dalamnya:
// Diimpor SplashScreen dari splashScreen.dart untuk digunakan sebagai halaman utama.
// MyApp adalah widget utama yang dijalankan saat aplikasi dimulai.
// Di dalam MyApp, menggunakan GetMaterialApp dari paket GetX sebagai root widget aplikasi. GetMaterialApp adalah subclass dari MaterialApp yang diperkaya dengan fitur-fitur dari GetX, seperti manajemen state, navigasi, dan lain-lain.
// Pada konfigurasi GetMaterialApp, debugShowCheckedModeBanner diatur ke false untuk menyembunyikan banner debug saat aplikasi dijalankan.
// Halaman utama (home) ditetapkan sebagai SplashScreen