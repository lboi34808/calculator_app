import 'package:flutter/material.dart';

// Kelas ButtonWidgetCalculator adalah StatelessWidget yang menampilkan tombol kalkulator dengan teks atau ikon.
class ButtonWidgetCalculator extends StatelessWidget {
  final IconData? icon;
  final Color backgroundColor;
  final Color foregroundColor;
  final String text;
  final VoidCallback? onTap;

// Konstruktor untuk membuat instance ButtonWidgetCalculator.
  const ButtonWidgetCalculator({
    Key? key,
    this.icon,
    required this.backgroundColor,
    required this.foregroundColor,
    required this.text,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
// GestureDetector untuk menangani ketika tombol ditekan.
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: backgroundColor,
        child: Center(
          child: icon == null
              ? Text(
                  text,
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: foregroundColor),
                )
              : Icon(
                  icon,
                  color: foregroundColor,
                ),
        ),
      ),
    );
  }
}
